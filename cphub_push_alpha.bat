@ECHO OFF
if [%1]==[] goto usage

set specfile="hub_spec.json"
set tag=%1
ECHO Pushing Ped Tracker Application version %1
cphub push -v -s %specfile% cpflexapp "./ped_tracker.bin" nali/applications/ped_tracker:%tag%
rem cphub push -v  application/text "README.md" nali/applications/ped_tracker:readme

goto :eof
:usage
@echo Usage: %0 ^<tag (e.g. v1.0.2.3)^>
exit /B 1
