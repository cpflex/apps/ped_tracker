echo off
if [%1]==[] goto usage

REM Specifications
set ocmfile="PedTrackerBasic.ocm.json"
set uripkg=//hub.cpflex.tech/nali/interfaces/ped_tracker_api
set tmpdir="ocmtemp"

REM Script compiles latest OCM Spec and pushes it to the hub.
set tag=%1
set uritag=%uripkg%:%tag%

echo ** Creating temp directory: %tmpdir%
mkdir %tmpdir%
copy *.ocm.json %tmpdir%
cd %tmpdir%

echo ** Compiling OCM Spec %ocmfile%
call ocmc compile %ocmfile% 

echo ** Pushing spec to CP-Flex Hub:  %uritag%
cphub push application/x-xmf specfile.ocm.xmf %uritag% -v
cphub push application/json  spec_%ocmfile% %uritag% -v

echo ** Updating Project Nid definitions.
copy /Y *.i ..\..\src

cd ..
rmdir /S /Q %tmpdir%

echo ** Publishing Complete.
goto :eof

:usage
@echo Usage: %0 ^<api tag (e.g. v1.0.2.3)^>
exit /B 1
