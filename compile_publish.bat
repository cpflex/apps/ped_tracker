@ECHO OFF
if [%1]==[] goto usage

echo ** Compiling Application
call compile %1

echo ** Publishing Application
call cphub_push %1

goto :eof
:usage
@echo Usage: %0 ^<tag (e.g. v1.0.2.3)^>
exit /B 1
