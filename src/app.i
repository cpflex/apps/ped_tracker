/**
 *  Name:  app.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 */

forward app_EnterPowerSaving(pctCharged);
forward app_ExitPowerSaving(pctCharged);
