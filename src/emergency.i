/**
 *  Name:  emergency.i
 *
 *  This module is the proprietary property of Codepoint Technologies
 *  Copyright (C) 2019 Codepoint Technologies
 *  All Rights Reserved
 */

forward  e911_Init();
forward  bool: e911_ToggleMode();
forward  e911_GetMode();

/**
* @brief can only be deactivated by the service host.
*/
forward  bool: e911_SetMode( bool: bEnable);

/**
* @brief Reports Status of e911.
*/
forward e911_ReceiveMsg( seqOut, msg);
